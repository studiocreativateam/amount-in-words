<?php

namespace SCTeam\AmountInWords;

class Converter
{
    private const WORDS = [
        'minus',

        [
            'zero',
            'jeden',
            'dwa',
            'trzy',
            'cztery',
            'pięć',
            'sześć',
            'siedem',
            'osiem',
            'dziewięć',
        ],

        [
            'dziesięć',
            'jedenaście',
            'dwanaście',
            'trzynaście',
            'czternaście',
            'piętnaście',
            'szesnaście',
            'siedemnaście',
            'osiemnaście',
            'dziewiętnaście',
        ],

        [
            'dziesięć',
            'dwadzieścia',
            'trzydzieści',
            'czterdzieści',
            'pięćdziesiąt',
            'sześćdziesiąt',
            'siedemdziesiąt',
            'osiemdziesiąt',
            'dziewięćdziesiąt',
        ],

        [
            'sto',
            'dwieście',
            'trzysta',
            'czterysta',
            'pięćset',
            'sześćset',
            'siedemset',
            'osiemset',
            'dziewięćset',
        ],

        [
            'tysiąc',
            'tysiące',
            'tysięcy',
        ],

        [
            'milion',
            'miliony',
            'milionów',
        ],

        [
            'miliard',
            'miliardy',
            'miliardów',
        ],

        [
            'bilion',
            'biliony',
            'bilionów',
        ],

        [
            'biliard',
            'biliardy',
            'biliardów',
        ],

        [
            'trylion',
            'tryliony',
            'trylionów',
        ],

        [
            'tryliard',
            'tryliardy',
            'tryliardów',
        ],

        [
            'kwadrylion',
            'kwadryliony',
            'kwadrylionów',
        ],

        [
            'kwintylion',
            'kwintyliony',
            'kwintylionów',
        ],

        [
            'sekstylion',
            'sekstyliony',
            'sekstylionów',
        ],

        [
            'septylion',
            'septyliony',
            'septylionów',
        ],

        [
            'oktylion',
            'oktyliony',
            'oktylionów',
        ],

        [
            'nonylion',
            'nonyliony',
            'nonylionów',
        ],

        [
            'decylion',
            'decyliony',
            'decylionów',
        ],
    ];

    public static function parse($amount)
    {
        $amount = explode('.', $amount);

        $zl = preg_replace('/[^-\d]+/', '', $amount[0]);
        $gr = preg_replace('/[^\d]+/', '', substr($amount[1] ?? 0, 0, 2));
        while (strlen($gr) < 2) {
            $gr .= '0';
        }

        return self::inWords($zl).' '.self::inflection(['złoty', 'złote', 'złotych'], $zl).
            (intval($gr) == 0 ? '' : ' '.self::inWords($gr).' '.self::inflection(['grosz', 'grosze', 'groszy'], $gr));
    }

    private static function inflection($inflections, $int)
    {
        $txt = $inflections[2];
        if ($int == 1) {
            $txt = $inflections[0];
        }
        $part = (int)substr($int, -1);
        $rest = $int % 100;
        if (($part > 1 && $part < 5) & !($rest > 10 && $rest < 20)) {
            $txt = $inflections[1];
        }
        return $txt;
    }

    private static function number($int)
    {
        $result = '';
        $j = abs((int)$int);

        if ($j == 0) {
            return self::WORDS[1][0];
        }
        $part = $j % 10;
        $decimals = ($j % 100 - $part) / 10;
        $hundreds = ($j - $decimals * 10 - $part) / 100;

        if ($hundreds > 0) {
            $result .= self::WORDS[4][$hundreds - 1].' ';
        }

        if ($decimals > 0) {
            if ($decimals == 1) {
                $result .= self::WORDS[2][$part].' ';
            } else {
                $result .= self::WORDS[3][$decimals - 1].' ';
            }
        }

        if ($part > 0 && $decimals != 1) {
            $result .= self::WORDS[1][$part].' ';
        }
        return $result;
    }

    private static function inWords($int)
    {
        $in = preg_replace('/[^-\d]+/', '', $int);
        $out = '';

        if (str_starts_with($in, '-')) {
            $in = substr($in, 1);
            $out = self::WORDS[0].' ';
        }

        $txt = str_split(strrev($in), 3);

        if ($in == 0) {
            $out = self::WORDS[1][0].' ';
        }

        for ($i = count($txt) - 1; $i >= 0; $i--) {
            $number = (int)strrev($txt[$i]);
            if ($number > 0) {
                if ($i == 0) {
                    $out .= self::number($number).' ';
                } else {
                    $out .= ($number > 1 ? self::number($number).' ' : '')
                        .self::inflection(self::WORDS[4 + $i], $number).' ';
                }
            }
        }
        return trim($out);
    }
}
